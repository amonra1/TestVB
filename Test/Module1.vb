﻿Module Module1
    Private Const MaxDevoir As Integer = 4

    Sub Main()
        Console.WriteLine("la moyenne est " & Enumerable.Repeat(1, MaxDevoir).Select(Function(i) CreeDevoir()).Average(Function(d) d.Note))
        Console.ReadLine()
    End Sub

    Private Function CreeDevoir() As Devoir
        Dim devoir = New Devoir(
            CDbl(GetInfo("Entrer une note")),
            GetInfo("Entrer un nom de prof"),
            GetInfo("Entrer un nom de devoir"),
            GetInfo("Comment tu t'intitule"))
        Console.WriteLine(devoir.message())
        Return devoir
    End Function

    Private Function GetInfo(question As String) As String
        Console.WriteLine(question)
        Return Console.ReadLine()
    End Function
End Module
