﻿Public Class Devoir

    Public Sub New(note As Double, nomProf As String, nomDevoir As String, monNom As String)
        Me.Note = note
        Me.NomProf = nomProf
        Me.NomDevoir = nomDevoir
        Me.MonNom = monNom
    End Sub

    Public ReadOnly Property Note As Double
    Public ReadOnly Property NomProf As String
    Public ReadOnly Property NomDevoir As String
    Public ReadOnly Property MonNom As String

    Public Function message() As String
        Return "la note : " & Note & " nom du devoir " & NomDevoir & " nom du Prof " & NomProf & " nom eleve " & MonNom
    End Function
End Class
